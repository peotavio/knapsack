# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 16:52:17 2015

@author: pedro.mello
"""
from search.GreedySearch import GreedySearch
from util.Cost import Cost

class Report(object):
    def __init__(self):
        pass
    
    def report1(self, K, nIterations, S):
        better, worst, equal = 0,0,0
        grChoice = GreedySearch().greedyChoice(K,S)
        costGrChoice = Cost().cost(grChoice)
        sumWiGrChoice = Cost().sumWi(grChoice)
        sumItemsGrChoice = sum(i[2] for i in iter(grChoice))
        grChoiceCostAndSum_Wi = (sumItemsGrChoice, costGrChoice, sumWiGrChoice),    
        grRandomChoiceCostAndSum_Wi = ()       
        for i in range(nIterations):
            grRandomChoice = GreedySearch().firstRandomChoice(K,S)
            costRandomChoice = Cost().cost(grRandomChoice)
            sumWiRandomChoice = Cost().sumWi(grRandomChoice)
            sumItemsRandomChoice = sum(i[2] for i in iter(grRandomChoice))
            grRandomChoiceCostAndSum_Wi += (sumItemsRandomChoice,costRandomChoice,sumWiRandomChoice),
            if(costRandomChoice > costGrChoice):
                better+=1
            elif(costRandomChoice == costGrChoice):
                equal+=1
            else:
               worst+=1
        print "="*10+" GREEDY CHOICE " +"="*10 
        print "Total Items in Knapsack".ljust(30) + "Cost".ljust(8) + "Sum(W_i)".ljust(8)
        for i in grChoiceCostAndSum_Wi:     
            print "{}".format(i[0]).ljust(30)+"{}".format(i[1]).ljust(8)+"{}".format(i[2]).ljust(8)
        print "="*10+" RANDOM FIRST CHOICE " +"="*10
        print "Total Items in Knapsack".ljust(30) + "Cost".ljust(8) + "Sum(W_i)".ljust(8)
        for i in grRandomChoiceCostAndSum_Wi:     
            print "{}".format(i[0]).ljust(30)+"{}".format(i[1]).ljust(8)+"{}".format(i[2]).ljust(8)
        print "Conclusoes: de {} iteracoes {} otimizaram, {} pioraram, {} tiveram resultados iguais".format(nIterations, better, worst, equal)
        print "Lucro Maximo encontrado: {}".format(max(i[1] for i in iter(grRandomChoiceCostAndSum_Wi)))        