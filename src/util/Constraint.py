'''
Created on 25/11/2015

@author: Pedro
'''

class Constraint(object):
    def __init__(self):
        pass
    
    def constraint1(self, K, feasibleItem = [], S=[]):
        """
        Modifica a relacao de pertinencia do item
        :return feasibleItem pertinence 1 ou 0
        """
        condition = (sum(i[0]*i[2] for i in iter(S)) + feasibleItem[0]) <= K 
        if(condition):
            feasibleItem[2] = bool(1)
        return feasibleItem

    def constraint2(self, K, feasibleItem = [], S=[]):
        """
        Avalia a restricao, para o caso de KP a restricao do somatorio dos pesos deve ser menor igual a K
        :param K: upper bound
        :param items: tuple of items
        :return boolean
        """
        return sum(i[0]*i[2] for i in iter(S)) + feasibleItem[0] <= K
    
    def constraint3(self, K, list1, list2):
        """
        Avalia a restricao de itens em duas listas de itens
        """
        sum1 = sum(i[0] for i in list1)
        sum2 = sum(i[0] for i in list2)
        if(sum1+sum2 <= K):
            tmp = list1+list2
            for i in tmp:
                i[2] = True
            return tmp