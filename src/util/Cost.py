'''
Created on 25/11/2015

@author: Pedro
'''

class Cost(object):
    def __init__(self):
        pass
    
    def cost(self, items=()):
        """
        Retorna o valor da funcao objetivo
        :param items: tuple of items
        :return cost
        """
        return sum(i[1]*i[2] for i in iter(items))
    
    def sumWi(self, items=()):
        return sum(i[0]*i[2] for i in iter(items))
