# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 13:06:13 2015

@author: Pedro
"""

import xml.etree.ElementTree as ET
from search.GreedySearch import GreedySearch
from search.LocalSearch import LocalSearch
from search.SimulatedAnnealing import SimulatedAnnealing

from util.Report import Report
from util.Cost import Cost
from chameleon.benchmark import benchmark


item = lambda w, g, pertinence: [w, g, pertinence]

def parseXMLToItems(filename):
    """
    PARSE XML TO AN ITEMS TUPLE  
    @return: an items list
    """
    items = []
    tree = ET.parse("scripts/{}".format(filename))
    root = tree.getroot()
    for child in root:
        w = int(child.find("size").text)
        g = int(child.find("profit").text)
        applicable = bool(0)
        items.append(item(w,g,applicable))
    return items

def grSearch():
    S = parseXMLToItems(filename)
    grSearch = GreedySearch()
    S_ = grSearch.greedyChoice(K,S)
    cost = Cost().cost(S_)
    sum_Wi = Cost().sumWi(S_)
    return tuple((S_,cost, sum_Wi))  

def firstRandomChoice():
    S = parseXMLToItems(filename)
    firstRandomChoice = GreedySearch()
    S_ = firstRandomChoice.firstRandomChoice(K,S)
    cost = Cost().cost(S_)
    sum_Wi = Cost().sumWi(S_)
    return tuple((S_,cost, sum_Wi))

def firstComparativeTest():
    print "="*10+" FIRST COMPARATIVE TEST GRSEARCH vs GR RANDOM CHOICE SEARCH"+"="*10 
    print "="*10+" GRSEARCH "+"="*10        
    resultGrSearch = grSearch()
    print "Total Items in Knapsack".ljust(30) + "cost".ljust(8)+"Sum(W_i)".ljust(8)  
    print "{}".format(sum(item[2] for item in resultGrSearch[0])).ljust(30)+"{}".format(resultGrSearch[1]).ljust(8)+"{}".format(resultGrSearch[2]).ljust(8)
    print "="*10+" FIRST RANDOM CHOICE "+"="*10
    resultFirstRandomChoice = []
    for i in range(50):    
        resultFirstRandomChoice.append(firstRandomChoice())
    print "Total Items in Knapsack".ljust(30) + "cost".ljust(8)+"Sum(W_i)".ljust(8)  
    for i in resultFirstRandomChoice:
        print "{}".format(sum(item[2] for item in i[0])).ljust(30)+"{}".format(i[1]).ljust(8)+"{}".format(i[2]).ljust(8)
    print "Lucro maximo encontrado: {}".format(max([i[1] for i in iter(resultFirstRandomChoice)]))
    print "Total resultados melhores: {}".format(sum(1 for i in iter(resultFirstRandomChoice) if i[1] > resultGrSearch[1]))
    print "Total resultados piores: {}".format(sum(1 for i in iter(resultFirstRandomChoice) if i[1] < resultGrSearch[1]))
    print "Total resultados iguais: {}".format(sum(1 for i in iter(resultFirstRandomChoice) if i[1] == resultGrSearch[1]))

def secondComparativeTest():
    print "="*10+" SECOND COMPARATIVE TEST GR SEARCH vs LOCAL SEARCH "+"="*10
    print "="*10+" GRSEARCH "+"="*10        
    resultGrSearch = grSearch()
    print "Total Items in Knapsack".ljust(30) + "cost".ljust(8)+"Sum(W_i)".ljust(8)  
    print "{}".format(sum(item[2] for item in resultGrSearch[0])).ljust(30)+"{}".format(resultGrSearch[1]).ljust(8)+"{}".format(resultGrSearch[2]).ljust(8)
    print "="*10+" LOCAL SEARCH "+"="*10
    localSearch()
     
def localSearch():
    f.write("\n")
    S = parseXMLToItems(filename)
    localSearch = LocalSearch(K, S)
    N = {}
#     s = [it for it in iter(grSearch()[0]) if (it[2] == bool(1))] #Pegando apenas os itens na mochila
    s = [it for it in iter(grSearch()[0])]
    sumCost = sum(i[1]*i[2] for i in iter(s))
    sumWi = sum(i[0]*i[2] for i in iter(s))
    N[sumCost] = [s,sumWi]
    benchMark = max(N)
    print "Total Items in Knapsack".ljust(30) + "cost".ljust(8)+"Sum(W_i)".ljust(8)
    f.write("Total Items in Knapsack".ljust(30) + "cost".ljust(8)+"Sum(W_i)".ljust(8))
    f.write("\n")
    result = localSearch.Neighborhood1(N[max(N)][0], [])
#     result = localSearch.Neighborhood(benchMark, N[max(N)][0], [],[])
    for it in result:
        tmp = sorted(it, key = lambda item: item[1]/item[0], reverse = True)
        sumCost = sum(i[1]*i[2] for i in tmp)
        sumWi = sum(i[0]*i[2] for i in tmp)
        totalInKP = sum(i[2] for i in tmp)
        N[sumCost] = [it,sumWi]
        print "{}".format(totalInKP).ljust(30)+"{}".format(sumCost).ljust(8)+"{}".format(sumWi)
        f.write("{}".format(totalInKP).ljust(30)+"{}".format(sumCost).ljust(8)+"{}".format(sumWi))
        f.write("\n")
    print "Lucro maximo encontrado: {}".format(max(N))
    f.write("Lucro maximo encontrado: {}".format(max(N)))
    f.write("\n")
    print "Total resultados melhores: {}".format(sum(1 for i in iter(N) if i > benchMark))
    f.write("Total resultados melhores: {}".format(sum(1 for i in iter(N) if i > benchMark)))
    f.write("\n")
    print "Total resultados piores: {}".format(sum(1 for i in iter(N) if i < benchMark))
    f.write("Total resultados piores: {}".format(sum(1 for i in iter(N) if i < benchMark)))
    f.write("\n")
    print "Total resultados iguais: {}".format(sum(1 for i in iter(N) if i == benchMark))
    f.write("Total resultados iguais: {}".format(sum(1 for i in iter(N) if i == benchMark)))
    f.write("\n\n")
    f.close()

def thirdComparativeTest():
    print "="*10+" THIRD COMPARATIVE TEST GR SEARCH vs SIMULATED ANNEALING "+"="*10
    print "="*10+" GRSEARCH "+"="*10        
    resultGrSearch = grSearch()
    print "Total Items in Knapsack".ljust(30) + "cost".ljust(8)+"Sum(W_i)".ljust(8)  
    print "{}".format(sum(item[2] for item in resultGrSearch[0])).ljust(30)+"{}".format(resultGrSearch[1]).ljust(8)+"{}".format(resultGrSearch[2]).ljust(8)
    print "="*10+" SIMULATED ANNEALING "+"="*10
    simulatedAnnealing()

def simulatedAnnealing():
    S = parseXMLToItems(filename)
    localSearch = LocalSearch(K, S)
    simulatedAnnealing = SimulatedAnnealing(K, S)
    s = [it for it in iter(grSearch()[0])]
    sumCost = sum(i[1]*i[2] for i in iter(s))
    sumWi = sum(i[0]*i[2] for i in iter(s))
    N = {}
    N[sumCost] = [s,sumWi]
    result = localSearch.Neighborhood1(N[max(N)][0], [])
    for it in result:
        tmp = sorted(it, key = lambda item: item[1]/item[0], reverse = True)
        sumCost = sum(i[1]*i[2] for i in tmp)
        sumWi = sum(i[0]*i[2] for i in tmp)
        totalInKP = sum(i[2] for i in tmp)
        N[sumCost] = [it,sumWi]
    energia = max(N)
    T = s[1]
    benchMark = energia
    for i in [it for it in N]:
        if ((N[i])[1] < T):
            T = (N[i])[1]
#             s = (N[i])[0]
            s = [list(it) for it in (N[i])[0]]
    print "Total Items in Knapsack".ljust(30) + "cost".ljust(8)+"Sum(W_i)".ljust(8)
    f.write("Total Items in Knapsack".ljust(30) + "cost".ljust(8)+"Sum(W_i)".ljust(8))
    f.write("\n")
    result = simulatedAnnealing.moves(energia, s, T)
    for it in result:
        tmp = sorted(it, key = lambda item: item[1]/item[0], reverse = True)
        sumCost = sum(i[1]*i[2] for i in tmp)
        sumWi = sum(i[0]*i[2] for i in tmp)
        totalInKP = sum(i[2] for i in tmp)
        N[sumCost] = [it,sumWi]
        print "{}".format(totalInKP).ljust(30)+"{}".format(sumCost).ljust(8)+"{}".format(sumWi)
        f.write("{}".format(totalInKP).ljust(30)+"{}".format(sumCost).ljust(8)+"{}".format(sumWi))
        f.write("\n")
    print "Lucro maximo encontrado: {}".format(max(N))
    f.write("Lucro maximo encontrado: {}".format(max(N)))
    f.write("\n")
    print "Total resultados melhores: {}".format(sum(1 for i in iter(N) if i > benchMark))
    f.write("Total resultados melhores: {}".format(sum(1 for i in iter(N) if i > benchMark)))
    f.write("\n")
    print "Total resultados piores: {}".format(sum(1 for i in iter(N) if i < benchMark))
    f.write("Total resultados piores: {}".format(sum(1 for i in iter(N) if i < benchMark)))
    f.write("\n")
    print "Total resultados iguais: {}".format(sum(1 for i in iter(N) if i == benchMark))
    f.write("Total resultados iguais: {}".format(sum(1 for i in iter(N) if i == benchMark)))
    f.write("\n\n")
    f.close()
    

if __name__ == '__main__':
#   S = Search Space
    K = 100
    print "CAPACIDADE DA MOCHILA K: {}".format(K)
    for i in range(10):
        filename =  "instanceItems%02d.xml"%(i)   
        S = parseXMLToItems(filename)
        print "\n\n\t TESTE: %02d"%(i)
        firstComparativeTest()
    raw_input("\ntecle enter para continuar\n")
    for i in range(10):
        filename =  "instanceItems%02d.xml"%(i)
        try:
            f = open('resultados/outputLocalSearch.txt', 'a')
        except:
            f = open('resultados/outputLocalSearch.txt', 'w')
        print "\n\n\t TESTE: %02d"%(i)
        f.write("\n\n\t TESTE: %02d \n"%(i))
        secondComparativeTest()
    raw_input("\ntecle enter para continuar\n")
    for i in range(10):
        filename =  "instanceItems%02d.xml"%(i)
        try:
            f = open('resultados/outputSimulatedAnnealing.txt', 'a')
        except:
            f = open('resultados/outputSimulatedAnnealing.txt', 'w')
        print "\n\n\t TESTE: %02d"%(i)
        f.write("\n\n\t TESTE: %02d \n"%(i))
        thirdComparativeTest()
#     filename =  "instanceItems01.xml"
#     firstComparativeTest()
#     secondComparativeTest()
#     try:
#         f = open('resultados/outputSimulatedAnnealing.txt', 'a')
#     except:
#         f = open('resultados/outputSimulatedAnnealing.txt', 'w')
#     thirdComparativeTest()