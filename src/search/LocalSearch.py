'''
Created on 07/12/2015

@author: Pedro
'''
import itertools
import numpy as np
from util.Constraint import Constraint

class LocalSearch(object):
    '''
    classdocs
    '''
    def __init__(self, K, S=[]):
        '''
        :param K: knapsack bound
        :S: Search Space
        '''
        self.K = K
        self.S = S
        self.constraints = Constraint()
      
    def Neighborhood1(self, s, N):
        totalItens = sum(i[2] for i in s)     
        s = tuple(tuple(i) for i in s)
        N.append(s)
        tmp = list(list(i) for i in s if i[2] == True)
        if (totalItens > 3):
            nSorted = (totalItens/2)-1
            for i in range(nSorted):
                idx = np.random.randint(0,len(tmp))
                del tmp[idx]
        for i in tmp:
            i[2] = False
        s_ = sorted(self.S, key = lambda item: item[1]/item[0], reverse = True) #carregar o espaco de busca ordenado
        s_ = [it for it in s_ if it not in tmp] #eliminando itens
        for i in tmp:
            i[2] = True
        while nSorted:
            for i in itertools.combinations(s_,nSorted):
                it = self.constraints.constraint3(self.K, tmp, list(i))
                if(it):
                    N.append(tuple(tuple(k) for k in it))
            nSorted-=1
        return N
        
    
    def Neighborhood(self, benchMark, s, visited, N, nAttempts = 2):
        '''
        First Better Neighbor
        :param s: feasible solution
        '''
        constraints = Constraint()
        if(nAttempts):
            s = tuple(tuple(i) for i in s)
            N.append(s)
            for i in N:
                visited = list(list(it) for it in iter(i) if it[2] == bool(1))            
            for i in visited:
                i[2] = bool(0)
            s_ = sorted(self.S, key = lambda item: item[1]/item[0], reverse = True) #carregar o espaco de busca ordenado
            s_ = [it for it in s_ if it not in visited] #eliminando itens ja visitados
            for i in visited:
                tmp = []
                i[2] = bool(1)
                tmp.append(i)
                for j in s_:
                    if(constraints.constraint2(self.K,j,tmp)):
                        j[2] = bool(1)
                        tmp.append(j)
                    else:
                        j[2] = bool(0)
                        tmp.append(j)
                N.append(tuple(tuple(k) for k in tmp))
            for i in N:
                print len(i)
            return self.Neighborhood(benchMark, tmp, visited, N, nAttempts-1)                
        return N
            
#         for i in range(nAttempts):
            
#         if nAttempts:
#             s = sorted(s, key = lambda item: item[1]/item[0], reverse = True) #ordenar para escolher o suposto melhor
#             s = tuple(tuple(i) for i in s)
#             print s
#             better = s[0]
#             tmp = list(list(i) for i in s if i[2] == bool(1))
#             for i in tmp:
#                 i[2] = bool(0)
#                 visited.append(i)
#             N.append(s)
#             s_ = sorted(self.S, key = lambda item: item[1]/item[0], reverse = True) #carregar o espaco de busca ordenado
#             s_ = [it for it in s_ if it[2] == bool(0)] #bug
#             s_ = [it for it in s_ if it not in visited] #eliminando itens ja visitados
#             s_.append(list(better))
#             for it in iter(s_):
#                 it = constraints.constraint1(self.K, it, s_)
#             obj = sum(i[1]*i[2] for i in iter(s_)) #soma do lucro como benchmark
#             if(obj < benchMark):
#                 return self.Neighborhood(benchMark, s_, visited, N, nAttempts-1)
#         return N