'''
Created on 08/12/2015

@author: Pedro
'''
import itertools
import numpy as np
from util.Constraint import Constraint

class SimulatedAnnealing(object):
    '''
    classdocs
    '''
    def __init__(self, K, S=[]):
        '''
        :param K: knapsack bound
        :param S: Search Space
        '''
        self.K = K
        self.S = S
        self.constraints = Constraint()
        
    def moves(self, energia, s, T, i = 50):
        """
        Movimenta a solucao
        :param energia: f(s)
        :param s: solucao
        :param T: temperatura
        :param i: numero de iteracoes
        """
        N = []
        for i in range(i):
            for j in s:
                j[2] = False
            s_ = sorted(self.S, key = lambda item: item[1]/item[0], reverse = True) #carregar o espaco de busca ordenado
            s_ = [it for it in s_ if it not in s] #eliminando itens do espaco de busca
            for j in s:
                j[2] = True
            while(T <= self.K):
                N.append(tuple(tuple(j) for j in s))
                s.append(min(s_)) #adicionando o item mais leve
                T = sum(it[0] for it in s)            
            if T > self.K:
                self.cooling(s, T)
                T = sum(it[0] for it in s)
        return N
    
    def cooling(self, s, T):
        idx = np.random.randint(0,len(s))
        del s[idx]