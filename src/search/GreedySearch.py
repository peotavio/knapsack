# -*- coding: utf-8 -*-
import numpy as np
from util.Constraint import Constraint

__author__ = 'Pedro'


class GreedySearch(object):
    def __init__(self):
        pass      
    
    def greedyChoice(self, K, S=[]):
        """
        Realiza a escolha dos itens sob o criterio guloso (VALOR/PESO)
        :param K: knapsack bound
        :param items: tuple of items
        :return: list of items which sum of sizes are restricted by K
        """
        constraints = Constraint()     
        sorted_S = sorted(S, key = lambda item: item[1]/item[0], reverse = True)      
        for it in iter(sorted_S):
            it = constraints.constraint1(K,it, sorted_S)
        return sorted_S


    def firstRandomChoice(self,K,S=[]):
        """
        Dada uma lista ordenada de elementos possíveis, o primeiro elemento sera escolhido de modo aleatorio
        e o restante das escolhas e obtido pelo metodo guloso.
        :param items: tuple of sorted items by greedy choice
        :return: list of items which the first element choice is random.
        """
        size = len(S)
        idx = np.random.randint(0,size)        
        S[idx][2] = bool(1)
        return self.greedyChoice(K,S)