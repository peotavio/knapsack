# -*- coding: utf-8 -*-
"""
Created on Sun Sep 27 13:26:20 2015

@author: Pedro
"""
import xml.etree.ElementTree as ET
import numpy as np

from datetime import date

class InstanceGenerator(object):
    def __init__(self, totalItems, weightLowerBound=None, weightUpperBound=None, profitUpperBound=None):
        '''
        Constructor
        '''
        self.totalItems = totalItems
        self.weightLowerBound = weightLowerBound
        self.weightUpperBound = weightUpperBound
        self.profitUpperBound = profitUpperBound
    
    def instanceGenerator(self, fileName):
        root = ET.Element("items")
        for i in range(self.totalItems):
            doc = ET.SubElement(root,"item")
            ET.SubElement(doc,"size").text = str(np.random.randint(self.weightLowerBound,self.weightUpperBound))
            ET.SubElement(doc,"profit").text = str(np.random.randint(1,self.profitUpperBound))
            ET.SubElement(doc,"pertinence").text = "0"
        tree = ET.ElementTree(root)
        tree.write(fileName+".xml")        
    
if __name__ == '__main__':
    totalItems = 100
#    Instancias sao criadas com peso 80% do total de itens
    knapsackCapacity = totalItems
#    knapsackCapacity = totalItems*1.8-totalItems
    weightLowerBound = ((5.0/100)*knapsackCapacity)
    weightUpperBound = knapsackCapacity - ((10.0/100)*knapsackCapacity)
    profitUpperBound = knapsackCapacity * 4
    instanceItems = InstanceGenerator(totalItems,weightLowerBound, weightUpperBound, profitUpperBound)
    for i in range(0,10):
        instanceItems.instanceGenerator("instanceItems%02d"%(i))
#     instance10Items = InstanceGenerator(totalItems,weightUpperBound, profitUpperBound)
#     instance10Items.instanceGenerator("instanceItems"+date.today().isoformat())
